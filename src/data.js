module.exports = {
  nav: [{
    text: '首页',
    path: 'index'
  }, {
    text: '案例展示',
    path: 'case'
  }, {
    text: '歌曲版权',
    path: 'copyright'
  }, {
    text:'在线报名',
    path: 'report',
  }, {
    text: '关于我们',
    path: 'about'
  }, {
    text: '联系我们',
    path: 'contact'
  }]
}
